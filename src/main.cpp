#include <Arduino.h>
#include <Hash.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ArduinoJson.h>
#include <WebSocketsServer.h>

#define WIFI_SSID "SSID"
#define WIFI_PWD "PASSWORD"

// #include "AppConfig.h"  // OVERWRITE WIFI SSID AND PASSWORD

ESP8266WiFiMulti WiFiMulti;
WebSocketsServer webSocket = WebSocketsServer(80);
uint8_t g_remoteIP;

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
  switch (type) {
    case WStype_DISCONNECTED:
      Serial.println("WStype_DISCONNECTE");
      break;
    case WStype_CONNECTED:
      Serial.println("WStype_CONNECTED");
      g_remoteIP = num;
      break;
    case WStype_TEXT:
      {
        Serial.println("WStype_TEXT");
        StaticJsonBuffer<512> jsonBuffer;
        JsonObject& jsonObj = jsonBuffer.parseObject(payload);
        if (!jsonObj.success()) {
          Serial.println("Failed to parse json string");
          Serial.println((char*)payload);
          break;
        }
        const char* message = jsonObj["message"];
        if (message == NULL) {
          Serial.println("Unexpected json message");
          Serial.println((char*)payload);
          break;
        }
        Serial.println(message);
        char replyBuffer[512];
        jsonObj.printTo(replyBuffer, sizeof(replyBuffer));
        Serial.println(replyBuffer);
        webSocket.sendTXT(g_remoteIP, replyBuffer);
      }
      break;
  }
}

void setup() {
  Serial.begin(115200);
  delay(1000);

  /* WiFi 接続 */
  WiFiMulti.addAP(WIFI_SSID, WIFI_PWD);

  while (WiFiMulti.run() != WL_CONNECTED) {
    delay(100);
  }

  Serial.print("WiFi connected: ");
  Serial.println(WiFi.localIP());

  /* WebSocketServer初期化 */
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
}

void loop() {
  webSocket.loop();
}
